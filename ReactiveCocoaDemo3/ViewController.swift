import UIKit
import ReactiveCocoa
import ReactiveSwift

class ViewController: UIViewController {

    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupBindings()
    }

    private func setupBindings() {
        let passwordSignal = passwordField
            .reactive
            .continuousTextValues
            .map { $0! }
        
        let confirmPasswordSignal = confirmPasswordField
            .reactive
            .continuousTextValues
            .map { $0! }
        
        let equalPasswords = passwordSignal
            .combineLatest(with: confirmPasswordSignal)
            .map { (password, confirm) -> Bool in
            return password == confirm
        }
        
        

        let passwordLengthIsValidSignal = passwordSignal.map { text -> Bool in
            return text.characters.count > 2
        }

        let passwordsValidSignal = equalPasswords
            .combineLatest(with: passwordLengthIsValidSignal)
            .map { (one, two) -> Bool in
                return one && two
        }

        let userTappedOvermatch = MutableProperty<Bool>(false)
        
        let didTapButtonSignal = nextButton.reactive.trigger(for: .touchUpInside)
        
        didTapButtonSignal
            .take(first: 3)
            .observeCompleted {
                userTappedOvermatch.swap(true)
        }
        
        nextButton.reactive.isEnabled <~ passwordsValidSignal
        nextButton.reactive.isEnabled <~ userTappedOvermatch.map(!)
        
        passwordField.reactive.isEnabled <~ userTappedOvermatch.map(!)
        confirmPasswordField.reactive.isEnabled <~ userTappedOvermatch.map(!)
        
    }
}

