import UIKit
import ReactiveCocoa
import ReactiveSwift

class ViewController2: UIViewController {
    var loadImageAction: Action<URL, UIImage?, AppError>!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loadButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLoadImageAction()
        setupBindings()
        
    }
    
    private func setupLoadImageAction() {
        loadImageAction = Action { url in
            return self.loadImageProducer(url: url)
                .observe(on: UIScheduler())
        }
        
        loadImageAction
            .events
            .observe { event in
                guard let image = event.value?.value else { return }
                self.imageView.image = image
        }
    }
    
    private func setupBindings() {
        loadButton.addTarget(self, action: #selector(didTouchButton), for: .touchUpInside)
        
        loadButton.reactive.isEnabled <~ loadImageAction.isExecuting.map(!)
        
        imageView.reactive.alpha <~ loadImageAction.isExecuting.map { $0 ? 0.5 : 1.0 }
        
        loadImageAction.isExecuting.signal.observe { event in
            guard let value = event.value else { return }
            UIApplication.shared.isNetworkActivityIndicatorVisible = value
        }
    }
    
    @objc private func didTouchButton() {
        loadImageAction
            .apply(URL(string: "http://lorempixel.com/400/200/")!)
            .start()
    }
    
    private func loadImageProducer(url: URL) -> SignalProducer<UIImage?, AppError> {
        let request = URLRequest(url: url)
        return URLSession.shared.reactive.data(with: request)
            .observe(on: QueueScheduler())
            .retry(upTo: 2)
            .map { (data, _) -> UIImage? in
                UIImage(data: data) }
            .mapError{ _ in AppError.defaultError }
    }
}

enum AppError: Error {
    case defaultError
}
